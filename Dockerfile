FROM node
WORKDIR /app
COPY /build .
EXPOSE 8312
CMD ["node", "index.js"]

