# Weather Chat

A dynamic Node server that receives a location and makes an API call to obtain weather. Made for testing Docker/Kubernetes.

## Install

`npm install`

## Develop

`npm run start`
May fail to actually trigger a rebuild/restart on file-change.
Consider running Prettier - eg., in VSCode install "Prettier - Code formatter" extension and turn on Settings->Editor->"Format On Save".

## Build

`npm run build`
`docker build -t registry.gitlab.com/zpeterg/weatherserv:1.0.0 .`
`docker push registry.gitlab.com/zpeterg/weatherserv:1.0.0`

## Run in Production

`docker run -d registry.gitlab.com/zpeterg/weatherserv:1.0.0`
