"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _https = _interopRequireDefault(require("https"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var callTo = function callTo(urlString) {
  return new Promise(function (resolve, reject) {
    _https["default"].get(urlString, function (resp) {
      var data = ""; // A chunk of data has been recieved.

      resp.on("data", function (chunk) {
        data += chunk;
      }); // The whole response has been received. Print out the result.

      resp.on("end", function () {
        //console.log("Data is -------", data)
        var jsonData = JSON.parse(data);
        resolve(jsonData);
      });
    }).on("error", function (err) {
      reject(err.message);
    });
  });
};

var _default = callTo;
exports["default"] = _default;