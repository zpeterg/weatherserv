"use strict";

var _http = _interopRequireDefault(require("http"));

var _https = _interopRequireDefault(require("https"));

var _url = _interopRequireDefault(require("url"));

var _callTo = _interopRequireDefault(require("./callTo"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var hostname = "127.0.0.1";
var port = 8312;

var server = _http["default"].createServer(function (req, res) {
  var path = _url["default"].parse(req.url).path.split("/"); // Fail if no query


  if (typeof path[2] === "undefined" || path[2] === "undefined") {
    res.statusCode = 404;
    res.end();
    return;
  }

  var urlLoc = "https://www.metaweather.com/api/location/search/?query=".concat(path[2]);
  (0, _callTo["default"])(urlLoc).then(function (locRes) {
    var urlWeather = "https://www.metaweather.com/api/location/".concat(locRes[0].woeid, "/");
    return (0, _callTo["default"])(urlWeather);
  }).then(function (weatherRes) {
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");
    var weather = weatherRes.consolidated_weather[0];
    res.end(JSON.stringify({
      weather: weather
    }));
  })["catch"](function (e) {
    console.log("ERROR:", e);
  });
});

server.listen(port, hostname, function () {
  console.log("Server running at http://".concat(hostname, ":").concat(port));
});