import https from "https"

const callTo = urlString => {
  return new Promise((resolve, reject) => {
    https
      .get(urlString, resp => {
        let data = ""

        // A chunk of data has been recieved.
        resp.on("data", chunk => {
          data += chunk
        })

        // The whole response has been received. Print out the result.
        resp.on("end", () => {
          //console.log("Data is -------", data)
          const jsonData = JSON.parse(data)
          resolve(jsonData)
        })
      })
      .on("error", err => {
        reject(err.message)
      })
  })
}

export default callTo
