import http from "http"
import https from "https"
import url from "url"
import callTo from "./callTo"

const hostname = "127.0.0.1"
const port = 8312

const server = http.createServer((req, res) => {
  const path = url.parse(req.url).path.split("/")
  // Fail if no query
  if (typeof path[2] === "undefined" || path[2] === "undefined") {
    res.statusCode = 404
    res.end()
    return
  }
  const urlLoc = `https://www.metaweather.com/api/location/search/?query=${
    path[2]
  }`
  callTo(urlLoc)
    .then(locRes => {
      const urlWeather = `https://www.metaweather.com/api/location/${
        locRes[0].woeid
      }/`
      return callTo(urlWeather)
    })
    .then(weatherRes => {
      res.statusCode = 200
      res.setHeader("Content-Type", "application/json")
      const weather = weatherRes.consolidated_weather[0]
      res.end(
        JSON.stringify({
          weather,
        }),
      )
    })
    .catch(e => {
      console.log("ERROR:", e)
    })
})

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}`)
})
